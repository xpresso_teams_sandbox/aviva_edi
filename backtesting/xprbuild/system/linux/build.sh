#! /bin/bash
## This script is used to build the project.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user
set -e

# Building xpresso dependencies
IFS=',' read -ra xpresso_dependencies_list <<< "${XPRESSO_DEPENDENCIES}"
for dep in "${xpresso_dependencies_list[@]}"; do
    cd ${ROOT_FOLDER}/../${dep}
    make install
done

python ${ROOT_FOLDER}/requirements/update_proxy.py

#apt-get update && apt-get install -y build-essential
#apt-get install -y libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
#cd /usr/src
#wget https://www.python.org/ftp/python/3.5.4/Python-3.5.4.tgz
#tar xzf Python-3.5.4.tgz
#cd Python-3.5.4
#./configure --enable-optimizations
#make altinstall

#apt-get install -y libatlas-base-dev gfortran pkg-config libfreetype6-dev
#python3.5 -m pip install numpy cython
#python3.5 -m pip install wheel
#python3.5 -m pip install -U setuptools

#apt-get install -y libhdf5-serial-dev
#python3.5 -m pip install tables
#python3.5 -m pip install zipline==1.3.0 
#python3.5 -m pip install xlrd matplotlib

#mv /usr/local/lib/python3.5/site-packages/zipline/data/benchmarks.py /usr/local/lib/python3.5/site-packages/zipline/data/benchmarks_old.py
#mv /usr/local/lib/python3.5/site-packages/zipline/data/treasuries.py /usr/local/lib/python3.5/site-packages/zipline/data/treasuries_old.py
#mv /usr/local/lib/python3.5/site-packages/zipline/data/loader.py /usr/local/lib/python3.5/site-packages/zipline/data/loader_old.py
#cp ${ROOT_FOLDER}/requirements/benchmarks.py /usr/local/lib/python3.5/site-packages/zipline/data/
#cp ${ROOT_FOLDER}/requirements/treasuries.py /usr/local/lib/python3.5/site-packages/zipline/data/
#cp ${ROOT_FOLDER}/requirements/loader.py /usr/local/lib/python3.5/site-packages/zipline/data/

# Build the dependencies
python3.5 -m pip install -r ${ROOT_FOLDER}/requirements/requirements.txt --force-reinstall

pip freeze | grep pandas
python3.5 -m pip freeze | grep pandas