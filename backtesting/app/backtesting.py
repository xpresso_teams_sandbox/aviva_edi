# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 16:56:06 2020

@author: dhenderson
"""

import sys
from zipline.api import record, symbol, set_benchmark, order_target_percent
import zipline
from datetime import datetime
import pandas as pd
from collections import OrderedDict
import pytz
import matplotlib.pyplot as plt
from trading_calendars import get_calendar


###############################
#### BACKTESTING FUNCTIONS ####
def initialize(context):
    set_benchmark(symbol("SPX"))

    # Initialise components list
    context.components = []
    # Parameters
    context.eq_short = {'data': symbol("SPX"), 'mom_window': int(sys.argv[2]),
                        'zscore_window': 500, 'zscore_method': 'EMA',
                        'weight': 0.5, 'transformations': ['lvl'],
                        'reverse': False}
    context.components.append(context.eq_short)

    context.eq_long = {'data': symbol("SPX"), 'mom_window': 240,
                       'zscore_window': 500, 'zscore_method': 'EMA',
                       'weight': 0.5, 'transformations': ['lvl'],
                       'reverse': False}
    context.components.append(context.eq_long)

    context.i = 0


def handle_data(context, data):
    # skip days to have fully initialised indicators
    context.i += 1
    if context.i < context.eq_long['mom_window'] + context.eq_long[
        'zscore_window']:
        return

    # Perform transformations and calculation of signal
    for component in context.components:
        # transform data
        component['transformed_data'] = get_data(data, context, component)

        # calc momentum
        component['mom'] = calc_mom(component['transformed_data'],
                                    component['mom_window'])

        # calc zscore
        component['signal'] = calc_zscore(component['mom'],
                                          component['zscore_window'],
                                          component['reverse'],
                                          component['zscore_method'])

    # Combine signals
    # EQ
    signal = context.eq_short['signal'] * context.eq_long['weight'] + \
             context.eq_long['signal'] * context.eq_long['weight']

    # exposure mapping
    if signal > 0:
        order_target_percent(symbol('SPX'), 1)
    elif signal < 0:
        order_target_percent(symbol('SPX'), 0)

    # Record raw data
    record(SPX=data.current(symbol('SPX'), 'close'))
    # Record momentum series
    record(eq_short_mom=context.eq_short['mom'].iloc[-1])
    record(eq_long_mom=context.eq_long['mom'].iloc[-1])
    # Record signals
    record(signal=signal)
    record(eq_short_signal=context.eq_short['signal'])
    record(eq_long_signal=context.eq_long['signal'])


def get_data(data, context, component):
    # Get data
    data_raw = data.history(component['data'], 'close', bar_count=context.i,
                            frequency='1d').dropna()
    # Do all data transformations
    for transformation in component['transformations']:
        if transformation == 'pct':
            data_raw = data_raw.pct_change()
        elif transformation == 'diff':
            data_raw = data_raw.diff()
        elif transformation == 'lvl':
            data_raw = data_raw
    return data_raw


def calc_mom(data, window, method='PriceMinus'):
    if method == 'SMA':
        return data.rolling(window).mean()
    elif method == 'EMA':
        return data.ewm(span=window).mean()
    elif method == 'PriceMinus':
        return data.iloc[-1] - data.rolling(window).mean()


def calc_zscore(data, window, reverse, method='EMA'):
    if method == 'SMA':
        zscore = ((data.iloc[-1]) / data.iloc[-window:].std())
    elif method == 'EMA':
        zscore = ((data.iloc[-1]) / data.ewm(span=window).std().iloc[-1])
    # bound signal
    if zscore > 3:
        signal = 3
    elif zscore < -3:
        signal = -3
    else:
        signal = zscore
    # check if we need to reverse
    if reverse:
        signal = -signal
    return signal


###############################
# DATA HANDLING FUNCTIONS

def convert_2_panel(data):
    panel = pd.Panel(data)
    panel.minor_axis = ["open", "high", "low", "close", "volume"]
    # Specify timezone
    panel.major_axis = panel.major_axis.tz_localize(pytz.utc)
    return panel


# Get data and transform to panel
run_name = sys.argv[1]
use_refinitiv_data = 'yes'
if use_refinitiv_data.lower() == 'true' or use_refinitiv_data.lower() == 'yes':
    print('Using Refinitiv data for backtesting...')
    #file_location = '/data/' + run_name + '/refinitiv_data.csv'
    #file_location = '/data/refinitiv_data_2gb.csv'
    file_location = '/data/refinitiv_data.csv'
    spx = pd.read_csv(file_location, index_col=0, parse_dates=True)    
else:
    print('Using manual data for backtesting...')
    spx = pd.read_excel('/data/SPX.xlsx', index_col=0, parse_dates=True)
data = OrderedDict()
data['SPX'] = spx
panel = convert_2_panel(data)
capital_base = 10000000
stock_exchange_code = 'NYSE'

# Run backtest
# This calls the zipline backtesting engine
perf = zipline.run_algorithm(start=datetime(2015, 3, 31, 0, 0, 0, 0, pytz.utc),
                             end=datetime(2020, 3, 27, 0, 0, 0, 0, pytz.utc),
                             initialize=initialize,
                             capital_base=capital_base,
                             handle_data=handle_data,
                             data=panel,
                             trading_calendar=get_calendar(stock_exchange_code))

print(perf)
print('Saving Benchmark and Portfolio output to csv...')
filename_benchmark = '/data/' + run_name + '--benchmark.csv'
filename_portfolio = '/data/' + run_name + '--portfolio.csv'
perf.portfolio_value.pct_change().fillna(0).add(1).cumprod().sub(1).to_csv(filename_portfolio)
perf.SPX.pct_change().fillna(0).add(1).cumprod().sub(1).to_csv(filename_benchmark)

##########
# Plotting
##########

# style.use("ggplot")
plt.figure(1)
perf.portfolio_value.pct_change().fillna(0).add(1).cumprod().sub(1).plot(
    label='portfolio')
perf.SPX.pct_change().fillna(0).add(1).cumprod().sub(1).plot(label='benchmark')
plt.legend(loc=2)
save_loc = '/data/' + run_name + '/first_plot.png'
plt.savefig(save_loc)

# Total Signal
plt.figure(2)
perf.signal.plot.area(stacked=False)
perf.SPX.plot(secondary_y=True)
save_loc2 = '/data/' + run_name + '/total_signal.png'
plt.savefig(save_loc2)
