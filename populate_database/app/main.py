"""
This is the implementation of data preparation for sklearn
"""

import csv
import mysql.connector as mysql
import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("populate_database",level=logging.INFO)


class PopulateDatabase(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="PopulateDatabase")
        """ Initialize all the required constansts and data her """

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            benchmark_file, portfolio_file, metadata_file = \
                self.create_csv_for_db(run_name)
            port = 32041
            db = 'Macrobond'
            main_table = 'SPXData'
            metadata_table = 'SPXMetaData'
            cnx = mysql.connect(user='root', password='abz00ba1nc',
                                host='10.0.6.11', port=port, database=db)
            if cnx.is_connected():
                print(f'Successfully connected to database "{db}"')
            else:
                print('ERROR: Connection attempt failed.')
            cursor = cnx.cursor(buffered=True)
            with open(portfolio_file, 'rt') as p, \
                    open(benchmark_file, 'rt') as b, \
                    open(metadata_file, 'rt') as m:
                for row_p in csv.reader(p):
                    cursor.execute(f"INSERT INTO {main_table} VALUES(%s,%s,%s)",
                                   (row_p[0], row_p[1], row_p[2]))
                for row_b in csv.reader(b):
                    cursor.execute(f"INSERT INTO {main_table} VALUES(%s,%s,%s)",
                                   (row_b[0], row_b[1], row_b[2]))
                for row_m in csv.reader(m):
                    cursor.execute(f"INSERT INTO {metadata_table} VALUES(%s,%s,%s)",
                                   (row_m[0], row_m[1], row_m[2]))
            cnx.commit()
            cursor.close()
            cnx.close()
            print('Data inserted successfully into the database.')

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def create_csv_for_db(self, run_name):
        """ converts backtesting results to suitable csv for pushing into db """
        benchmark_csv = '/data/' + run_name + '--benchmark.csv'
        portfolio_csv = '/data/' + run_name + '--portfolio.csv'
        benchmark_db_file = '/data/' + run_name + '--benchmark--db.csv'
        portfolio_db_file = '/data/' + run_name + '--portfolio--db.csv'
        metadata_db_file = '/data/' + run_name + '--metadata.csv'
        benchmark_identifier = run_name + '_benchmark'
        portfolio_identifier = run_name + '_portfolio'
        with open(benchmark_csv, 'rt') as rb, \
                open(benchmark_db_file, 'wt+') as wb:
            writer = csv.writer(wb)
            for row_number, row_data in enumerate(csv.reader(rb)):
                datetime = row_data[0]
                date = datetime.split(' ')[0]
                writer.writerow([benchmark_identifier, date, row_data[1]])
        with open(portfolio_csv, 'rt') as rp, \
                open(portfolio_db_file, 'wt+') as wp:
            writer = csv.writer(wp)
            for row_index, data in enumerate(csv.reader(rp)):
                datetime = data[0]
                date = datetime.split(' ')[0]
                writer.writerow([portfolio_identifier, date, data[1]])
        with open(metadata_db_file, 'wt+') as m:
            writer = csv.writer(m)
            benchmark_description = 'Benchmark identifier for run ' + run_name
            portfolio_description = 'Portfolio identifier for run ' + run_name
            frequency = 'Daily'
            writer.writerow([portfolio_identifier, portfolio_description,
                             frequency])
            writer.writerow([benchmark_identifier, benchmark_description,
                             frequency])
        return benchmark_db_file, portfolio_db_file, metadata_db_file


    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = PopulateDatabase()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
