CREATE DATABASE IF NOT EXISTS Macrobond;
USE Macrobond;

CREATE TABLE IF NOT EXISTS SPXData(
        identifier VARCHAR(255) NOT NULL,
        date DATE NOT NULL,
        value FLOAT(10,6)
);

CREATE TABLE IF NOT EXISTS SPXMetaData(
        identifier VARCHAR(255) NOT NULL,
        description VARCHAR(255),
        frequency VARCHAR(255) NOT NULL
);

SHOW TABLES;
DESCRIBE SPXData;
DESCRIBE SPXMetaData;